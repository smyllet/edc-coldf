public class Adherent extends Personne {
    private Adresse uneAdresse ;

    public Adherent(String nom, String prenom, String telPersonne, Adresse uneAdresse) {
        super(nom, prenom, telPersonne);
        this.uneAdresse = uneAdresse;
    }

    public Adresse getUneAdresse() {
        return uneAdresse;
    }

    public void setUneAdresse(Adresse uneAdresse) {
        this.uneAdresse = uneAdresse;
    }

    public String getAdressePostale() {
        return uneAdresse.getNumero() + " - " + uneAdresse.getNomRue() + " - " + uneAdresse.getCodePostal() + " - " + uneAdresse.getVille();
    }
}
