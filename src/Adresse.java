public class Adresse {
    private String nomRue, codePostal, ville ;
    private int numero ;

    public Adresse(String nomRue, String codePostal, String ville, int numero) {
        this.nomRue = nomRue;
        this.codePostal = codePostal;
        this.ville = ville;
        this.numero = numero;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
