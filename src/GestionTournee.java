import java.util.ArrayList;
import java.util.List;

public class GestionTournee {
    private Tournee laTournee;
    // Constructeur
    public GestionTournee(Tournee uneTournee) {
        this.laTournee = uneTournee;
    }

    // Ajoute la visite uneVisite à la liste des visites de la tournée
    public void ajouterVisite(Visite uneVisite) {
        laTournee.ajouterVisite(uneVisite);
    }
    // Retourne la liste des adhérents visités sur la tournée
    // un adhérent n'est visité qu'une seule fois au cours de la tournée
    public List<Adherent> getAdherents() {
        List<Adherent> adherents = new ArrayList<>();
        for(Visite visite : laTournee.getLesVisites()) {
            adherents.add(visite.getLeAdherent());
        }
        return adherents;
    }

    // Retourne le montant total à facturer pour l’ensemble des visites de la tournée
    public float CATournee() {
        float caTournee = 0;

        for(Visite visite : laTournee.getLesVisites()) {
            caTournee += visite.montantAFacturer();
        }

        return caTournee;
    }

    // Retourne le montant total  à facturer pour l’ensemble des visites de la tournée
    // correspondant au type de prestation passé en paramètre
    public float CATournee(NaturePrestation uneNaturePrestation) {
        return 0 ;
    }
}
