public class Inseminateur extends Personne {
    private String login, password ;

    public Inseminateur(String nom, String prenom, String telPersonne, String login, String password) {
        super(nom, prenom, telPersonne);
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
