public class NaturePrestation {
    private String libelle;
    private float prixBase ;

    public NaturePrestation(String libelle, float prixBase) {
        this.libelle = libelle;
        this.prixBase = prixBase;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrixBase() {
        return prixBase;
    }

    public void setPrixBasee(float prixBase) {
        this.prixBase = prixBase;
    }
}
