public class Personne {
    private String nom, prenom, telPersonne ;

    public Personne(String nom, String prenom, String telPersonne) {
        this.nom = nom;
        this.prenom = prenom;
        this.telPersonne = telPersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelPersonne() {
        return telPersonne;
    }

    public void setTelPersonne(String telPersonne) {
        this.telPersonne = telPersonne;
    }
}
