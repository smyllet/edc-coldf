public class PrestationVisite {
    private NaturePrestation laNaturePrestation;  // la nature de prestation effectuée durant la visite
    private int nombreFacture;

    public PrestationVisite(NaturePrestation laNaturePrestation, int nombreFacture) {
        this.laNaturePrestation = laNaturePrestation;
        this.nombreFacture = nombreFacture;
    }

    public NaturePrestation getNaturePrestationn() {
        return laNaturePrestation;
    }

    public void setNaturePrestation(NaturePrestation laNaturePrestation) {
        this.laNaturePrestation = laNaturePrestation;
    }

    public int getnombreFacture() {
        return nombreFacture;
    }

    public void setnombreFacture(int nombreFacture) {
        this.nombreFacture = nombreFacture;
    }
}
