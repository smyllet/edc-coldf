import java.util.*;
import java.util.stream.Collectors;

public class SyntheseTournee {

    List<Tournee> listeTourneesExaminees ;

    // Liste de tournées du jour


    public SyntheseTournee() {
        listeTourneesExaminees = new ArrayList<>( );
    }

    public SyntheseTournee( List<Tournee> listeTourneesExaminees) {
        this.listeTourneesExaminees = listeTourneesExaminees;
    }



    public List<Tournee> getListeTourneesExaminees() {
        return listeTourneesExaminees;
    }

    public void setListeTourneesExaminees(List<Tournee> listeTourneesExaminees) {
        this.listeTourneesExaminees = listeTourneesExaminees;
    }

    // A COMPLETER SUR VOTRE COPIE
	
	// Méthode qui retourne la liste des tournées pour un jour donné, parmi les tournées associées à la classe SyntheseTournee
    public List<Tournee> tourneesDunJour(Date date) {
        List<Tournee> tournees = new ArrayList<>();

        for(Tournee tournee : listeTourneesExaminees) {
            if(tournee.getDate().equals(date)) tournees.add(tournee);
        }

        return tournees;

        // Solution alternatif via un stream
        //return listeTourneesExaminees.stream().filter(tournee -> tournee.getDate().equals(date)).collect(Collectors.toList());
    }

    // A COMPLETER SUR VOTRE COPIE
	// Méthode qui doit permettre de connaître pour chaque adhérent le nombre de visites associées pour un jour donné
    public Map<Adherent, Integer> comptageAdherent(Date date) {
        Map<Adherent, Integer> tournees = new HashMap<>();

        for(Tournee tournee : tourneesDunJour(date))
            for(Visite visite : tournee.getLesVisites())
                tournees.merge(visite.getLeAdherent(), 1, Integer::sum);

        return tournees;
    }
}
