import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Tournee {
    private Date date;
    private int kmRealises;  // nombre de km effectués pendant la tournée
    private Inseminateur leInseminateur;
    private List<Visite> lesVisites ;

    public Tournee(Date date, Inseminateur leInseminateur) {
        this.date = date;
        this.leInseminateur = leInseminateur;
        lesVisites = new ArrayList<>() ;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getKmRealises() {
        return kmRealises;
    }

    public void setKmRealises(int kmRealises) {
        this.kmRealises = kmRealises;
    }

    public Inseminateur getLeInseminateur() {
        return leInseminateur;
    }

    public void setLeInseminateur(Inseminateur leInseminateur) {
        this.leInseminateur = leInseminateur;
    }

    public List<Visite> getLesVisites() {
        return lesVisites;
    }

    public void setLesVisites(List<Visite> lesVisites) {
        this.lesVisites = lesVisites;
    }

    public void ajouterVisite(Visite uneVisite) {
        if (!lesVisites.contains(uneVisite))
            lesVisites.add(uneVisite) ;
    }
}
