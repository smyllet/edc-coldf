import java.util.ArrayList;
import java.util.List;

public class Visite {
    private Adherent leAdherent;
    private String heure;  // sous la forme hh:mm par exemple "09:00"
    private List<PrestationVisite> lesPrestationsVisite;

    public Visite(Adherent leAdherent, String heure,List<PrestationVisite> lesPrestationsVisite) {
        this.leAdherent = leAdherent;
        this.heure = heure;
        this.lesPrestationsVisite = lesPrestationsVisite;
    }

    public Adherent getLeAdherent() {
        return leAdherent;
    }

    public void setLeAdherent(Adherent leAdherent) {
        this.leAdherent = leAdherent;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public List<PrestationVisite> getLesPrestationsVisite() {
        return lesPrestationsVisite;
    }

    public void setLesPrestationsVisite(List<PrestationVisite> lesPrestationsVisite) {
        this.lesPrestationsVisite = lesPrestationsVisite;
    }

    public void ajoutPrestationVisite(NaturePrestation uneNaturePrestation, int unNombreActes) {
        lesPrestationsVisite.add(new PrestationVisite(uneNaturePrestation, unNombreActes));
    }

    // Ajout d'une méthode ajoutPrestationVisite avec un seul paramètre
    public void ajoutPrestationVisite(PrestationVisite prestationVisite) {
       lesPrestationsVisite.add(prestationVisite);
    }

    public float montantAFacturer(){
        float montantFacture = 0;

        for(PrestationVisite prestationVisite : lesPrestationsVisite) {
            montantFacture += prestationVisite.getNaturePrestationn().getPrixBase() * prestationVisite.getnombreFacture();
        }

        return montantFacture;
    }
}
