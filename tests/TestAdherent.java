import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAdherent {
    private Adresse adr1 ;
    private Adherent adh1;

    @BeforeEach
    public void setUp() throws Exception {
        adr1 = new Adresse("Carnot","94700","Maisons-Alfort",41) ;
        adh1 = new Adherent("Ionescu", "Georges", "0600000020", adr1);
    }

    @Test
    public void testAdressePostale() {
        assertEquals("41 - Carnot - 94700 - Maisons-Alfort", adh1.getAdressePostale(), "Adresse Postal incorrect");
    }
}
