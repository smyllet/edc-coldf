import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TestGestionTournee {
    private Inseminateur ins1;
    private Adherent adh1, adh2;
    private Adresse adr1, adr2 ;
    private Visite v1, v2;
    private NaturePrestation np1, np2;
    private Tournee t1;
    private GestionTournee gt;

    @BeforeEach
    public void setUp() throws Exception {
        np1 = new NaturePrestation("Insémination", 80);
        np2 = new NaturePrestation("Echographie", 20);
        ins1 = new Inseminateur("Robert", "Patrick", "0600000100","admin","admin");
        adr1 = new Adresse("Carnot","94700","Maisons-Alfort",41) ;
        adr2 = new Adresse("Silvestri","94300","Vincennes",23) ;
        adh1 = new Adherent("Ionescu", "Georges", "0600000020", adr1);
        adh2 = new Adherent("Renard", "Marguerite", "0600000003", adr2);
        t1 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020"), ins1);
	    gt = new GestionTournee(t1);
    }

	@Test
    public void testGetAdherents() {
        assertThat("La liste des adhérent n'est pas contient des éléments en trop à l'initialisation", gt.getAdherents(), not(hasItems(adh1, adh2)));

        gt.ajouterVisite(new Visite(adh1, "08:50", null));
        assertThat("L'adhérent 1 n'est pas retourné après que ça visite ai été ajouté", gt.getAdherents(), hasItem(adh1));
        assertThat("L'adhérent 2 est retourné alors que ça visite n'a pas été ajouté", gt.getAdherents(), not(hasItem(adh2)));

        gt.ajouterVisite(new Visite(adh2, "09:40", null));
        assertThat("Les adhérent de la tournée ne sont pas tous retourné", gt.getAdherents(), hasItems(adh1, adh2));
    }
	
    @Test
    public void testCATournee() {
        // Vérification montant total initial (un float ne peux pas être null)
        assertEquals(0, gt.CATournee(), "Le montant total initial n'est pas à 0");

        // Vérification montant total après l'ajout de la première visite
        v1 = new Visite(adh1,"09h30", new ArrayList<>());
        v1.ajoutPrestationVisite(np1, 2);
        v1.ajoutPrestationVisite(np2, 1);
        gt.ajouterVisite(v1);

        assertEquals(180, gt.CATournee(), "Montant total après la première visite est incorrect");

        // Vérification montant total final
        v2 = new Visite(adh2,"11h30", new ArrayList<>());
        v2.ajoutPrestationVisite(np1, 2);
        gt.ajouterVisite(v2);

        assertEquals(340, gt.CATournee(), "Montant total final est incorrect");
    }
}
