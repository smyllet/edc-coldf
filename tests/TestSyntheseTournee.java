import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSyntheseTournee {

    private Inseminateur ins1, ins2;
    private Adherent adh1, adh2;
    private Adresse adr1, adr2 ;
    private Visite v1, v2, v3;
    private PrestationVisite p1, p2, p3, p4, p5 ;
    private NaturePrestation np1, np2, np3, np4;
    private Tournee t1, t2,t3, t4, t5;
    private SyntheseTournee syntheseTournee ;

    @BeforeEach
    public void setUp() throws Exception {
        np1 = new NaturePrestation("Insémination", 80);
        np2 = new NaturePrestation("Echographie", 20);
        np3 = new NaturePrestation("Controle", 5);
        np4 = new NaturePrestation("Diagnostic approfondi", 30);


        ins1 = new Inseminateur("Petit", "Ferdinand", "0600000100","admin","admin");
        ins2 = new Inseminateur("Roi", "Rodolphe", "0600000105","rodolphe","rodolphe");

        adr1 = new Adresse("Carnot","94700","Maisons-Alfort",41) ;
        adr2 = new Adresse("Silvestri","94300","Vincennes",23) ;
        adh1 = new Adherent("Duboeuf", "Georges", "0600000020", adr1);
        adh2 = new Adherent("Renard", "Marguerite", "0600000003", adr2);

        t1 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020"), ins1);
        t2 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020"), ins1);
        t3 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020"), ins2);
        t4 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020"), ins2);
        t5 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020"), ins2);



        PrestationVisite p1 = new PrestationVisite(np1,1) ;
        t1.ajouterVisite(new Visite(adh1,"9h", Arrays.asList(p1)));

        PrestationVisite p2 = new PrestationVisite(np2,1) ;
        PrestationVisite p3 = new PrestationVisite(np3,1) ;

        t2.ajouterVisite(new Visite(adh1,"12h", Arrays.asList(p2,p3)));

        PrestationVisite p4 = new PrestationVisite(np3,1) ;
        PrestationVisite p5 = new PrestationVisite(np4,1) ;
        t3.ajouterVisite(new Visite(adh1,"16h", Arrays.asList(p4,p5)));

        t4.ajouterVisite(new Visite(adh1,"9h", Arrays.asList(p4,p5)));

        t5.ajouterVisite(new Visite(adh2,"9h", Arrays.asList(p1,p2)));


        syntheseTournee = new SyntheseTournee( Arrays.asList(t1,t2,t3,t4,t5)) ;
    }

    @Test
    public void testTourneesDunJour() throws Exception {
        assertThat("Les 3 premiers tournée ne sont pas retourné pour le 6 mai 2020",
                    syntheseTournee.tourneesDunJour(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020")),
                    hasItems(t1, t2, t3));

        assertThat("Les 4eme et 5eme tournée sont retourné pour le 6 mai 2020",
                    syntheseTournee.tourneesDunJour(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020")),
                    not(hasItems(t4, t5)));

        assertThat("Les 3 premiers tournée sont retourné pour le 7 mai 2020",
                    syntheseTournee.tourneesDunJour(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020")),
                    not(hasItems(t1, t2, t3)));

        assertThat("Les 4eme et 5eme tournée ne sont pas retourné pour le 7 mai 2020",
                    syntheseTournee.tourneesDunJour(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020")),
                    hasItems(t4, t5));
    }

    @Test
    public void testComptageAdherent() throws Exception {
        assertEquals(syntheseTournee.comptageAdherent(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020")).size(),
                    1,
                    "Le nombre d'élément retourné pour le 6 mai 2020 est incorrect");

        assertEquals(syntheseTournee.comptageAdherent(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020")).size(),
                2,
                "Le nombre d'élément retourné pour le 7 mai 2020 est incorrect");


        assertEquals(syntheseTournee.comptageAdherent(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020")).get(adh1),
                3,
                "Le nombre de visite pour l'adhérent 1 le 6 mai est incorrect");

        assertEquals(syntheseTournee.comptageAdherent(new SimpleDateFormat("dd-MM-yyyy").parse("07-05-2020")).get(adh2),
                1,
                "Le nombre de visite pour l'adhérent 2 le 7 mai est incorrect");
    }
}
