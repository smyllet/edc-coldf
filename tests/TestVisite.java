import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestVisite {
    private Inseminateur ins1;
    private Adherent adh1;
    private Adresse adr1 ;
    private Visite v1;
    private NaturePrestation np1, np2;
    private Tournee t1;


    @BeforeEach
    public void setUp() throws Exception {
        np1 = new NaturePrestation("Insémination", 80);
        np2 = new NaturePrestation("Echographie", 20);
        ins1 = new Inseminateur("Robert", "Patrick", "0600000100","admin","admin");
        adr1 = new Adresse("Carnot","94700","Maisons-Alfort",41) ;
        adh1 = new Adherent("Ionescu", "Georges", "0600000020", adr1);
        t1 = new Tournee(new SimpleDateFormat("dd-MM-yyyy").parse("06-05-2020"), ins1);

    }

    @Test
    public void testMontantAFacturer() {
        v1 = new Visite(adh1,"9h30", new ArrayList<>());

        assertEquals(0, v1.montantAFacturer(), "Montant à facturé incorrect");

        PrestationVisite p1 = new PrestationVisite(np1,3) ;
        v1.ajoutPrestationVisite(p1);

        assertEquals(240, v1.montantAFacturer(), "Montant à facturé incorrect");

        PrestationVisite p2 = new PrestationVisite(np2,1) ;
        v1.ajoutPrestationVisite(p2);

        assertEquals(260, v1.montantAFacturer(), "Montant à facturé incorrect");
    }

}
